use std::io::ErrorKind;
use std::net::UdpSocket;
use std::time::Duration;

use bitvec::order::{Lsb0, Msb0};
use bitvec::{bitvec, slice::BitSlice};
use gpio_cdev::Chip;

use flipdot::{FlipdotArray, PinConfig, TrackingFlipdotArray};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let socket = UdpSocket::bind("[::]:2323")?;
    socket.set_nonblocking(true)?;

    let mut array = TrackingFlipdotArray::new(FlipdotArray::from_env()?);

    let mut buf = vec![255u8; array.total_pixels().div_ceil(8)];
    loop {
        let mut count = 0;
        let mut received = 0;
        loop {
            match socket.recv(&mut buf) {
                Ok(new_count) => {
                    count = new_count;
                    received += 1;
                },
                Err(e) if e.kind() == ErrorKind::WouldBlock => break,
                Err(e) => Err(e)?
            }
        }
        if received > 1 {
            eprintln!("Dropped {} frames.", received - 1);
        }
        if received == 0 {
            std::thread::sleep(Duration::from_millis(1));
            continue;
        }
        let mut image = bitvec![usize, Lsb0; 1; array.total_pixels()];
        let mut n = 0usize;
        for bit in BitSlice::<u8, Msb0>::from_slice(&buf[0..count]) {
            let image_x = n % array.pixels_y();
            let image_y = n / array.pixels_y();
            let array_x = image_y;
            let array_y = (array.pixels_y() - 1) - image_x;
            let array_index = array_x + array_y * array.pixels_x();
            image.set(array_index, *bit);
            n += 1;
        }
        array.update(&image);
    }
}
