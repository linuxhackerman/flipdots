use std::thread::sleep;
use std::time::{Duration, Instant};

use anyhow::Context;
use bitvec::array::BitArray;
use bitvec::vec::BitVec;
use bitvec::{bitarr, bitvec, BitArr};
use bitvec::{order::Lsb0, slice::BitSlice};
use gpio_cdev::{Chip, LineHandle, LineRequestFlags};

/// Definition of the numbers of the GPIOs that a flipdot assembly is
/// attached to.
pub struct PinConfig {
    power: u32,
    data_col: u32,
    data_row: u32,
    strobe: u32,
    oe_white: u32,
    oe_black: u32,
    clk_col: u32,
    clk_row: u32,
}

/// These defaults are based on the setup used in the raspberry pi
/// setup in muccc's schaufenster flipdot assembly.
impl Default for PinConfig {
    fn default() -> PinConfig {
        PinConfig {
            power: 17,
            data_col: 2,
            data_row: 3,
            strobe: 18,
            oe_white: 27,
            oe_black: 24,
            clk_col: 25,
            clk_row: 11,
        }
    }
}

/// Open lines for a shift register.
struct ShiftRegister {
    clk: LineHandle,
    data: LineHandle,
}

impl ShiftRegister {
    /// Push the bits from the slice into the register. Note that this
    /// may be more or less than the actual capacity of the hardware,
    /// which is considered irrelevant.
    fn push(&mut self, value: &BitSlice) {
        for bit in value {
            self.data
                .set_value(*bit as u8)
                .expect("writing to data pin");
            self.clk.set_value(1).expect("setting clk pin high");
            self.clk.set_value(0).expect("setting clk pin low");
        }
    }
}

/// Handles for all the control hardware for a flipdot assembly.
struct Pins {
    power: LineHandle,
    strobe: LineHandle,
    oe_white: LineHandle,
    oe_black: LineHandle,
    row_select: ShiftRegister,
    row_data: ShiftRegister,
}

/// Timing that can be dependent on the number of pixels changed.
pub enum SmartTiming {
    /// Always delay by the same amount.
    Static(Duration),
    /// Delay for a minimum of `min` and a maximum of `max`,
    /// interpolating linearly between them based on the proportion of
    /// pixels changed.
    ChangeDependent { min: Duration, max: Duration },
}

impl SmartTiming {
    /// Delay for when all the pixels change.
    fn apply_full(&self) -> Duration {
        match self {
            SmartTiming::Static(d) => *d,
            SmartTiming::ChangeDependent { min, max } => *max,
        }
    }
    /// Delay for when proportion/256 pixels are changing.
    fn apply_partial(&self, proportion: u8) -> Duration {
        match self {
            SmartTiming::Static(d) => *d,
            SmartTiming::ChangeDependent { min, max } => {
                let (min, max) = (*min, *max);
                let ret = min + (max - min) * proportion.into() / 256;
                ret
            }
        }
    }
}

pub struct Timing {
    /// Time for which the white output pin should be enabled (the
    /// electromagnets energised for flipping pixels to the white
    /// position). This should be increased if pixels are failing to
    /// flip to white, but kept as low as possible for lower power
    /// consumption and higher frame rates.
    pub output_enable_white: SmartTiming,

    /// Time for which the white output pin should be enabled (the
    /// electromagnets energised for flipping pixels to the black
    /// position). This should be increased if pixels are failing to
    /// flip to black, but kept as low as possible for lower power
    /// consumption and higher frame rates.
    pub output_enable_black: SmartTiming,

    /// Delay after enabling the power supply before we can assume the
    /// panels are actually powered.
    pub power_on: Duration,
}

impl Default for Timing {
    fn default() -> Timing {
        Timing {
            output_enable_white: SmartTiming::ChangeDependent {
                min: Duration::from_micros(300),
                max: Duration::from_micros(2000),
            },
            output_enable_black: SmartTiming::ChangeDependent {
                min: Duration::from_micros(300),
                max: Duration::from_micros(2000),
            },
            power_on: Duration::from_micros(2000),
        }
    }
}

/// Handle to a flipdot assembly which can be updated, but does not
/// track its state.
///
/// Note that the logical X dimension may not be physically
/// horizontal. The logical X dimension is the one along which panels
/// have 20 simultaneously updatable pixels. Panels have 16
/// simultaneously updatable rows of pixels along the Y dimension,
/// which can only be updated one at a time.
pub struct FlipdotArray {
    /// Number of panels along the logical X dimension.
    panels_x: u8,
    /// Number of panels along the logical Y dimension.
    panels_y: u8,
    /// Handles to the controlling pins.
    pins: Pins,
    /// Timing configuration.
    pub timing: Timing,
}

impl FlipdotArray {
    pub fn open(
        chip: &mut Chip,
        config: &PinConfig,
        timing: Timing,
        panels_x: u8,
        panels_y: u8,
    ) -> Result<FlipdotArray, gpio_cdev::Error> {
        let flags = LineRequestFlags::OUTPUT;
        let pins = Pins {
            power: chip
                .get_line(config.power)?
                .request(flags.clone(), 0, "flipdot")?,
            strobe: chip
                .get_line(config.strobe)?
                .request(flags.clone(), 0, "flipdot")?,
            oe_white: chip
                .get_line(config.oe_white)?
                .request(flags.clone(), 0, "flipdot")?,
            oe_black: chip
                .get_line(config.oe_black)?
                .request(flags.clone(), 0, "flipdot")?,
            row_data: ShiftRegister {
                clk: chip
                    .get_line(config.clk_row)?
                    .request(flags.clone(), 0, "flipdot")?,
                data: chip
                    .get_line(config.data_row)?
                    .request(flags.clone(), 0, "flipdot")?,
            },
            row_select: ShiftRegister {
                clk: chip
                    .get_line(config.clk_col)?
                    .request(flags.clone(), 0, "flipdot")?,
                data: chip
                    .get_line(config.data_col)?
                    .request(flags.clone(), 0, "flipdot")?,
            },
        };

        Ok(FlipdotArray {
            panels_x,
            panels_y,
            pins,
            timing,
        })
    }

    pub fn from_env() -> anyhow::Result<FlipdotArray> {
        let panels_x: u8 = std::env::var("FLIPDOT_PANELS_X")
            .context("getting FLIPDOT_PANELS_X from env")?
            .parse()
            .context("parsing FLIPDOT_PANELS_X from env")?;
        let panels_y: u8 = std::env::var("FLIPDOT_PANELS_Y")
            .context("getting FLIPDOT_PANELS_Y from env")?
            .parse()
            .context("parsing FLIPDOT_PANELS_Y from env")?;
        let mut chip = Chip::new("/dev/gpiochip0").context("Opening GPIO chip")?;
        FlipdotArray::open(
            &mut chip,
            &Default::default(),
            Default::default(),
            panels_x,
            panels_y,
        )
        .context("getting pins")
    }

    /// The size of the row-data shift register in bits.  Each panel
    /// consists of 16 rows of 20 pixels, but has 24 bits worth of
    /// shift register, so we have to pad the row data out with 4
    /// extra bits per panel. This represents the size (in bits) of
    /// the total row data for the whole array.
    fn raw_row_size(&self) -> usize {
        24 * self.panels_x as usize * self.panels_y as usize
    }

    /// Total number of panels in the array.
    pub fn total_panels(&self) -> usize {
        self.panels_x as usize * self.panels_y as usize
    }

    /// Total number of pixels in the array.
    pub fn total_pixels(&self) -> usize {
        self.total_panels() * 16 * 20
    }

    fn row_size(&self) -> usize {
        self.total_panels() * 20
    }

    pub fn pixels_x(&self) -> usize {
        self.panels_x as usize * 20
    }

    pub fn pixels_y(&self) -> usize {
        self.panels_y as usize * 16
    }

    /// Clears the array, making all pixels black.
    pub fn black(&mut self) {
        let row_data = bitvec![usize, Lsb0; 1; self.raw_row_size()];
        for row in 0..16 {
            self.select_row(row);
            self.pins.row_data.push(row_data.as_bitslice());
            self.strobe();
            self.start_black();
            sleep(self.timing.output_enable_black.apply_full());
            self.stop_output();
        }
    }

    /// Clears the array, making all pixels white.
    pub fn white(&mut self) {
        let row_data = bitvec![usize, Lsb0; 0; self.raw_row_size()];
        for row in 0..16 {
            self.select_row(row);
            self.pins.row_data.push(row_data.as_bitslice());
            self.strobe();
            self.start_white();
            sleep(self.timing.output_enable_white.apply_full());
            self.stop_output();
        }
    }

    fn write_row_data(&mut self, data: &BitSlice, row: usize) {
        let padding = bitvec![usize, Lsb0; 0; 4];
        let pixels_x = self.pixels_x();
        for panel_y in 0..self.panels_y as usize {
            for panel_x in 0..self.panels_x as usize {
                self.pins.row_data.push(padding.as_bitslice());
                let from_offset = panel_y * 16 * pixels_x + row * pixels_x + panel_x * 20;
                self.pins
                    .row_data
                    .push(&data[from_offset..from_offset + 20]);
            }
        }
    }

    /// Sets a given set of pixels. `pixels_white` is a bit slice,
    /// where 0 represents white and 1 represents no
    /// change. `pixels_to_black` is a bit slice where 0 represents no
    /// change and 1 represents black.
    pub fn update_pixels(&mut self, pixels_to_white: &BitSlice, pixels_to_black: &BitSlice) {
        let pixels_x = self.pixels_x();
        if pixels_to_white.len() != self.total_pixels() {
            panic!(
                "Not enough pixels in white buffer (expected {}, got {})",
                self.total_pixels(),
                pixels_to_white.len()
            );
        }
        if pixels_to_black.len() != self.total_pixels() {
            panic!(
                "Not enough pixels in black buffer (expected {}, got {})",
                self.total_pixels(),
                pixels_to_white.len()
            );
        }
        struct RowStats {
            nwhite: usize,
            nblack: usize,
            delay_white: Duration,
            delay_black: Duration,
        }
        let row_stats: Vec<_> = (0..16)
            .map(|row| {
                let mut nwhite = 0;
                let mut nblack = 0;
                for panel_y in 0..self.panels_y as usize {
                    let offset = panel_y * 16 * pixels_x + row * pixels_x;
                    let range = offset..offset + pixels_x;
                    nwhite += pixels_to_white[range.clone()].count_zeros();
                    nblack += pixels_to_black[range].count_ones();
                }
                RowStats {
                    nwhite,
                    nblack,
                    delay_white: self
                        .timing
                        .output_enable_white
                        .apply_partial((nwhite * 255 / pixels_x) as u8),
                    delay_black: self
                        .timing
                        .output_enable_black
                        .apply_partial((nblack * 255 / pixels_x) as u8),
                }
            })
            .collect();

        for row in 0..16 {
            self.select_row(row as u8);
            let row_data = &row_stats[row];
            if row_data.nwhite > 0 {
                self.write_row_data(pixels_to_white, row);
                self.strobe();
                self.start_white();
                sleep(row_data.delay_white);
                self.stop_output();
            }

            if row_data.nblack > 0 {
                self.write_row_data(pixels_to_black, row);
                self.strobe();
                self.start_black();
                sleep(row_data.delay_black);
                self.stop_output();
            }
        }
    }

    fn select_row(&mut self, row: u8) {
        let mut reg_value = bitvec![usize, Lsb0; 0; 16];
        reg_value.set(row.into(), true);
        self.pins.row_select.push(reg_value.as_bitslice());
    }

    fn strobe(&mut self) {
        self.pins.strobe.set_value(0).expect("strobe pin");
        self.pins.strobe.set_value(1).expect("strobe pin");
    }

    fn start_black(&mut self) {
        self.pins.oe_white.set_value(0).expect("oe_white");
        self.pins.oe_black.set_value(1).expect("oe_black");
    }
    fn start_white(&mut self) {
        self.pins.oe_black.set_value(0).expect("oe_black");
        self.pins.oe_white.set_value(1).expect("oe_white");
    }
    fn stop_output(&mut self) {
        self.pins.oe_white.set_value(0).expect("oe_white");
        self.pins.oe_black.set_value(0).expect("oe_black");
    }

    pub fn power_on(&mut self) {
        self.pins.power.set_value(1).expect("power");
        std::thread::sleep(self.timing.power_on);
    }
    pub fn power_off(&mut self) {
        self.pins.power.set_value(0).expect("power");
    }
}

/// A structure which tracks the currently displayed pixels in a
/// flipdot array and performs partial updates when given a new image
/// to display.
pub struct TrackingFlipdotArray {
    array: FlipdotArray,
    contents: BitVec,
}

impl TrackingFlipdotArray {
    pub fn new(mut array: FlipdotArray) -> TrackingFlipdotArray {
        let contents = bitvec![usize, Lsb0; 1; array.total_pixels()];
        array.power_on();
        array.black();
        array.power_off();
        TrackingFlipdotArray { array, contents }
    }

    /// Display a new image on the array. `image` should be a bitslice
    /// containing the pixels in (logical) left-to-right,
    /// top-to-bottom order. Only pixels which have changed from the
    /// previously displayed image will actually be activated.
    pub fn update(&mut self, image: &BitSlice) {
        assert!(image.len() == self.array.total_pixels());
        let mut to_white = !BitVec::from_bitslice(image);
        let mut to_black = !BitVec::from_bitslice(image);
        to_white |= !BitVec::from_bitslice(&self.contents);
        to_black &= !BitVec::from_bitslice(&self.contents);
        self.array.power_on();
        self.array.update_pixels(&to_white, &to_black);
        self.array.power_off();
        self.contents = !BitVec::from_bitslice(image);
    }

    pub fn total_pixels(&self) -> usize {
        self.array.total_pixels()
    }
    pub fn pixels_x(&self) -> usize {
        self.array.pixels_x()
    }
    pub fn pixels_y(&self) -> usize {
        self.array.pixels_y()
    }
}
