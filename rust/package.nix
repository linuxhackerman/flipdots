{ rustPlatform }:
rustPlatform.buildRustPackage {
  pname = "flipdot";
  version = "unstable";
  src = ./.;
  cargoLock.lockFile = ./Cargo.lock;
}
