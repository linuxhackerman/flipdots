#!/usr/bin/env python3

from FlipdotAPI.FlipdotMatrix import FlipdotMatrix
import sys
import os
import string
import time

import requests

def getHtml():
    return requests.get("https://muc.pads.ccc.de/ep/pad/export/flipdot/latest?format=txt").text

def run():
    old_html = ""
    while True:
        old_html = run_once(old_html)
        time.sleep(2.0)

def run_once(old_html = ""):
    html = getHtml()[0:1000]
    if (html != old_html):
        matrix = FlipdotMatrix()
        matrix.showText("wiki.muc.ccc.de/flipdot:33c3\n"+html)
    return html

#main
if (__name__=="__main__"):
    sys.setrecursionlimit(2028)
    run()

