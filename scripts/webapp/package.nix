{ python, buildPythonApplication, flask, flipdot, wrapPython, pillow, fetchFromGitHub }:
buildPythonApplication {
  pname = "flipdot-webapp";
  version = "unstable";
  src = ./.;
  nativeBuildInputs = [ wrapPython ];
  propagatedBuildInputs = [
    flask
    flipdot
    pillow
  ];
  format = "other";
  installPhase = ''
    mkdir -p $out/${python.sitePackages} $out/bin $out/share/flipdot/static
    install -Dm0755 webapp.py $out/share/flipdot/webapp.py
    ln -s $out/share/flipdot/webapp.py $out/bin/flipdot-webapp
    ln -sn ${
      fetchFromGitHub {
        owner = "muccc";
        repo = "flipdot-web-editor";
        rev = "5d37efcea530faf5f2f1f43f2e74d634710ada61";
        sha256 = "sha256-bRrr27RySVErOBwaer2ZTQugj4/Ln6bsL51J74aEIWw=";
      }
    } $out/share/flipdot/static/flipdot-web-editor
  '';

  postFixup = ''
    wrapPythonProgramsIn $out/share/flipdot $out
  '';
}
