{ python, buildPythonPackage, pillow, pytz, requests, numpy, pygame, gevent, flask }:
buildPythonPackage {
  pname = "flipdot-scripts";
  version = "unstable";
  src = ./.;
  propagatedBuildInputs = [
    pillow
    pytz
    requests
    numpy
    pygame
    gevent
    gevent
    flask
  ];
  format = "other";

  installPhase = ''
    mkdir -p $out/${python.sitePackages} $out/bin
    cp -r FlipdotAPI $out/${python.sitePackages}/
    cp fliputils.py $out/${python.sitePackages}/
    for script in *; do
        [[ -f $script ]] || continue
        head -n1 $script | grep -q python3 || continue
        install -Dm0755 "$script" "$out/bin/flipdot-''${script%.py}"
    done
  '';
}
