{ python, buildPythonPackage }:
buildPythonPackage {
  pname = "flipdot-lib";
  version = "unstable";
  src = ./.;
  format = "other";

  installPhase = ''
    mkdir -p $out/${python.sitePackages} $out/bin $out/share
    cp -r FlipdotAPI $out/${python.sitePackages}/
    cp fliputils.py $out/${python.sitePackages}/
  '';
}
