{
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
  inputs.flake-parts = {
    url = "github:hercules-ci/flake-parts";
    inputs.nixpkgs.follows = "nixpkgs";
  };
  inputs.fenix = {
    url = "github:nix-community/fenix";
    inputs.nixpkgs.follows = "nixpkgs";
  };
  outputs = inputs@{ flake-parts, ... }: flake-parts.lib.mkFlake { inherit inputs; } {
    systems = [ "x86_64-linux" ];
    flake.overlays.default = self: super: {
      flipdot-rust = super.callPackage ./rust/package.nix {};
      flipdot-webapp = self.python3.pkgs.callPackage ./scripts/webapp/package.nix {};
      flipdot-scripts = self.python3.pkgs.callPackage ./scripts/package.nix {};
      python3 = super.python3.override {
        packageOverrides = pyself: pysuper: {
          flipdot = super.newScope pyself ./scripts/pylib.nix {};
        };
      };
    };
    perSystem = { pkgs, inputs', self', ... }: let
      rustEnv = with inputs'.fenix.packages; combine [
        stable.cargo
        stable.rustc
        stable.rustfmt
        stable.rust-analyzer
        stable.rust-src
        stable.rust-std
        targets.arm-unknown-linux-gnueabihf.stable.rust-std
        targets.aarch64-unknown-linux-gnu.stable.rust-std
      ];
    in {
      devShells.default = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [
          clang-tools bear
          python3 python3.pkgs.pillow
          cargo-watch
          rustEnv
        ];
        CARGO_TARGET_ARM_UNKNOWN_LINUX_GNUEABIHF_LINKER = with pkgs.pkgsCross.raspberryPi.stdenv; "${cc}/bin/${cc.targetPrefix}cc";
        CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER = with pkgs.pkgsCross.aarch64-multiplatform.stdenv; "${cc}/bin/${cc.targetPrefix}cc";
      };
      packages = (inputs.self.overlays.default (pkgs // self'.packages) pkgs) // {
        default = self'.packages.flipdot;
      };
    };
  };
}
